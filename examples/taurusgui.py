import sys
from taurus.external.qt import Qt
from taurus.qt.qtgui.panel import TaurusForm
from taurus.qt.qtgui.display import TaurusLabel
from taurus.qt.qtgui.application import TaurusApplication
from taurus.qt.qtgui.container import TaurusWidget
from taurus.qt.qtgui.taurusgui import TaurusGui
from powermeter import PowerMeter2
from taurus_pyqtgraph import TaurusTrend

class OpenClosedLabel(TaurusLabel):
    def displayValue(self, value):
        return 'Closed' if value == 0 else 'Open'
    

class TrainingMainWidget(TaurusWidget):
    def __init__(self, parent=None, designMode=False):
        TaurusWidget.__init__(self, parent, designMode)
        self.setLayout(Qt.QGridLayout())

        form = TaurusForm()
        form.model = ["sys/tg_test/1/ampli", "sys/tg_test/1/double_scalar", "sys/tg_test/1/boolean_scalar", "sys/tg_test/1/state"]
        form.withButtons = False
        form.setMinimumHeight(150)
        self.layout().addWidget(form, 0, 0)

        ampli_taurus_value = form[0]
        ampli_taurus_value.readWidget().setStyleSheet("border: 3px solid")

        open_closed_label = OpenClosedLabel()
        open_closed_label.setModel("sys/tg_test/1/boolean_scalar")        
        self.layout().addWidget(open_closed_label, 0, 1)

        vertical_layout = Qt.QVBoxLayout()
        self.layout().addLayout(vertical_layout, 1, 0)

        expert_form = TaurusForm()
        expert_form.model = ["sys/tg_test/1/short_scalar"]
        expert_form.setEnabled(False)
        vertical_layout.addWidget(expert_form)
        self.expert_form = expert_form

        button = Qt.QCheckBox()
        button.setText("Expert")
        button.clicked.connect(self.setExpert)
        vertical_layout.addWidget(button)

        progressbar = PowerMeter2()
        progressbar.setModel("eval:Q(60+20*rand())")  # implicit use of  key="power"
        progressbar.setModel("eval:['green','red','blue'][randint(3)]", key="color")       
        self.layout().addWidget(progressbar, 1, 1)

        tabs = Qt.QTabWidget()
        trend1 = TaurusTrend()
        trend1.setModel(["eval:rand()", "eval:rand()"])
        self.curve_count = 0
        trend2 = TaurusTrend()
        trend2.setModel(["sys/tg_test/1/short_scalar", "sys/tg_test/1/long_scalar"])
        self.random_trend = trend1
        tabs.addTab(trend1, "Random trend")
        tabs.addTab(trend2, "Attributes trend")
        self.layout().addWidget(tabs, 2, 0)

        button = Qt.QPushButton()
        button.setText("Add random curve")
        button.clicked.connect(self.addRandomCurve)
        self.layout().addWidget(button, 2, 1)


    def setExpert(self, checked):
        self.expert_form.setEnabled(checked)

    def addRandomCurve(self):
        self.curve_count +=1
        self.random_trend.addModels("eval:rand()+{}".format(self.curve_count))

    
class TrainingTaurusGui(TaurusGui):
    def __init__(self, parent=None):
        TaurusGui.__init__(self, parent)
        mainwidget = TrainingMainWidget()

        #Setting mainwidget as Panel makes it movable and can be disabled
        self.createPanel(mainwidget, 'Main Widget', floating=False,
                          permanent=True)
        #Setting mainwidget as CentralWidget will make it always visible and unmovable
        #self.setCentralWidget(self.mainwidget)

        panel_form = TaurusForm()
        panel_form.setModel(["sys/tg_test/1/state", "sys/tg_test/1/status"])
        self.createPanel(panel_form, 'Another Panel', floating=False,
                          permanent=True)

app = TaurusApplication(sys.argv)
form = TrainingTaurusGui()

form.show()
sys.exit(app.exec_())