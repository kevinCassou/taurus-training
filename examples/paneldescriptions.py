from taurus.qt.qtgui.taurusgui.utils import PanelDescription

GUI_NAME = "taurus_paneldescriptions"
ORGANIZATION = "TAURUS"

# =========================================================================
# Specific logo. It can be an absolute path,or relative to the app dir or a
# resource path. If commented out, ":/taurus.png" will be used
# =========================================================================
# CUSTOM_LOGO = <path GUI-specific logo

# =========================================================================
# You can provide an URI for a manual in html format
# (comment out or make MANUAL_URI=None to skip creating a Manual panel)
# =========================================================================
MANUAL_URI = "http://www.taurus-scada.org"

# ===============================================================================
# If you want to have a main synoptic panel, set the SYNOPTIC variable
# to the file name of a jdraw file. If a relative path is given, the directory
# containing this configuration file will be used as root
# (comment out or make SYNOPTIC=None to skip creating a synoptic panel)
# ===============================================================================
SYNOPTIC = None

# ===============================================================================
# Set INSTRUMENTS_FROM_POOL to True for enabling auto-creation of
# instrument panels based on the Pool Instrument info
# ===============================================================================
INSTRUMENTS_FROM_POOL = True

# ===============================================================================
# Define panels to be shown.
# To define a panel, instantiate a PanelDescription object (see documentation
# for the gblgui_utils module)
# ===============================================================================

tango_host="tango://localhost:10000"
tango_test = f"{tango_host}/sys/tg_test/1"


taurus_form = PanelDescription("TaurusForm1",
                                classname="taurus.qt.qtgui.panel:TaurusForm",
                                model=f"{tango_test}/ampli")


taurus_trend = PanelDescription("TaurusTrend1",
                                 classname="taurus_pyqtgraph:TaurusTrend",
                                 model=[f"{tango_test}/double_scalar", f"{tango_test}/short_scalar"])

motors = PanelDescription("Motors",
                      classname="taurus.qt.qtgui.panel:TaurusForm",
                      model=[f"{tango_host}/motor/motctrl01/1", f"{tango_host}/motor/motctrl01/2", f"{tango_host}/motor/motctrl01/3/Position"])


attr_form = PanelDescription(
    "BigInstrument",
    classname="taurus.qt.qtgui.panel:TaurusAttrForm",
    model="sys/tg_test/1",
)

# custom_panel = PanelDescription("Custom",
#                           classname="MonoE",
#                           modulename="beamline.gui.mono_energy",
#                           model="blXX/di/energy-01")

# ===============================================================================
# Macro execution configuration
# (comment out or make MACRO_SERVER=None to skip creating a macro execution
# infrastructure)
# ===============================================================================

#MACROSERVER_NAME = "MacroServer/test/1"
#MACRO_PANELS = True
