import sys
from taurus.qt.qtgui.application import TaurusApplication
from taurus.qt.qtgui.panel import TaurusForm

app = TaurusApplication(sys.argv, app_name="Taurus Widget Training")
form = TaurusForm()
form.model = ["sys/tg_test/1/ampli", "sys/tg_test/1/double_scalar", "sys/tg_test/1/state"]
form.withButtons = False

ampli_taurus_value = form[0]
ampli_taurus_value.readWidget().setStyleSheet("border: 3px solid")

state_taurus_value = form[2]
state_taurus_value.setExtraWidget("taurus.qt.qtgui.button.taurusbutton:TaurusCommandButton")
#Setting the model to a device when originally it was an attribute shows an error in console.
#This does not affect the correct behavior, although it will be investigated in the future.
state_taurus_value.extraWidget().setModel("sys/tg_test/1")
state_taurus_value.extraWidget().setCommand("SwitchStates")

form.show()
sys.exit(app.exec_())