# Taurus Programatic GUI Development


## Introduction
Let's start with a deeper usage of taurus, which requires coding knowledge. Some scientists may know how to code and are able to make their own programatic GUIs, but this kind of GUIs are suposed to be developed and mantained by controls engineers (or similar roles) due to their complexity.

## Panel Description
This is the simpliest way of creating programatic GUIs. It consists on a .py configuration file that determines the default, permanent, pre-defined contents of the GUI. While the user may add/remove more elements at runtime and these customizations will also be stored, this file defines what a user will find when launching the GUI for the first time.  
It works similar to a xml configuration file generated with taurus newgui, but it's more flexible as it provides the PanelDescription API.  
>The GUI configuration will be saved at .config/{ORGANIZATION}/{GUI_NAME.ini} or in .config/Taurus/{filename.ini} if the ORGANIZATION and GUI_NAME are not defined.

### Simple example
1. Create a new paneldescriptionsgui.py config file.  
2. Import PanelDescription and create a couple PanelDescriptions like the following code:

```
from taurus.qt.qtgui.taurusgui.utils import PanelDescription

taurus_form = PanelDescription('TaurusForm1',
                                classname='taurus.qt.qtgui.panel:TaurusForm',
                                model='tango://localhost:10000/sys/tg_test/1/ampli')

taurus_trend = PanelDescription('TaurusTrend1',
                                 classname='taurus_pyqtgraph:TaurusTrend',
                                 model=['tango://localhost:10000/sys/tg_test/1/double_scalar', 'tango://localhost:10000/sys/tg_test/1/short_scalar'])
```
3. Run _taurus gui paneldescriptionsgui.py_. The panels will appear randomly located.
4. Move the panels to get something like this:  

<p align="center">
    <img src="img/paneldescriptions_simple.png" alt="paneldescriptions_simple" />
</p>

### Complex example
The GUI can have all the parameters that a GUI offer (the ones asked when running _taurus newgui_), like the GUI name, organization, synoptics...  
A bigger example with most of these parameters can be found [here](examples/paneldescriptions.py) and you can test it with _taurus gui paneldescriptions.py_.  
It may look like this after moving the panels:  
<p align="center">
    <img src="img/paneldescriptions_complex.png" alt="paneldescriptions_complex" />
</p>

## TaurusWidget
You can create and show single widgets with code. As a "simple" widget, it won't include all the features that a TaurusApplication provides (perspectives, create and move panels, add new models in runtime...). However, it may be useful for simple static GUIs that can be customized through code.  
All Taurus widgets inherit TaurusBaseWidget, which adds the capability of setting a model property.

### Simple example
We are going to show a simple TaurusForm (it could be any Taurus widget or a custom one that inherits TaurusBaseWidget). This widget will be inside a [TaurusApplication](https://taurus-scada.org/devel/api/taurus.qt.qtgui.application-TaurusApplication.html), which essentially is a Qt Application with some Taurus-specific additions, like the GUI name or Organization parameters.
1. Create a new tauruswidgetgui.py file.  
2. Import sys, TaurusApplication and TaurusForm.
3. Create a TaurusApplication and the TaurusForm. Add the model to the form, show it and execute the application. The code should be similar to:
```
import sys
from taurus.qt.qtgui.application import TaurusApplication
from taurus.qt.qtgui.panel import TaurusForm

app = TaurusApplication(sys.argv)
form = TaurusForm()
form.model = ['sys/tg_test/1/ampli', 'sys/tg_test/1/double_scalar']

form.show()
sys.exit(app.exec_())
```
4. Run _python tauruswidgetgui.py_. The widget will load looking like this:

<p align="center">
    <img src="img/tauruswidget_simple.png" alt="tauruswidget_simple" />
</p>

### Complex example
The programatic way to create the TaurusWidget allows you to customize it as you want at all levels: Taurus-specific (like model), TaurusWidget-specific (like withButtons in case of TaurusForm) and QtWidget (enabled/disabled, styles...). This example also shows how to set the GUI_NAME when creating the TaurusApplication.  
A bigger example with some of these parameters can be found [here](examples/tauruswidget.py) and you can test it with _python tauruswidget.py_. It may look like this:  
<p align="center">
    <img src="img/tauruswidget_complex.png" alt="tauruswidget_complex" />
</p>

## TaurusMainWindow
TaurusMainWindow is a Taurus-aware QMainWindow with several customizations:  
    - It takes care of (re)storing its geometry and state  
    - Supports perspectives, and allows defining a set of "factory settings"  
    - It provides a customizable splashScreen (optional)  
    - Supports spawning remote consoles and remote debugging  
    - Supports full-screen mode toggling  
    - Supports adding launchers to external applications  
    - It provides a statusBar with an optional heart-beat LED  
    - The following Menus are optionally provided and populated with basic actions:
    <p style="margin-left:2rem"> 
        - File  (accessible by derived classes  as `self.fileMenu`)  
        - View (accessible by derived classes  as `self.viewMenu`)  
        - Taurus (accessible by derived classes  as `self.taurusMenu`)  
        - Tools (accessible by derived classes  as `self.toolsMenu`)  
        - Help (accessible by derived classes  as `self.helpMenu`)  

### Simple example
1. Create a new taurusmainwindowgui.py file.  
2. Create a taurus application and a widget.
3. Set that widget as the central widget of the window using _setCentralWidget(widget)_.
4. Show the main window and execute the application. The code should look like this:

```
import sys
from taurus.qt.qtgui.panel import TaurusForm
from taurus.qt.qtgui.application import TaurusApplication
from taurus.qt.qtgui.container import TaurusMainWindow

app = TaurusApplication(sys.argv, app_name="Taurus Main Window Training")
mainwindow = TaurusMainWindow()

form = TaurusForm()
form.model = ["sys/tg_test/1/ampli", "sys/tg_test/1/double_scalar"]
mainwindow.setCentralWidget(form)

mainwindow.show()
sys.exit(app.exec_())
```
5. Run _python taurusmainwindowgui.py_. The GUI will load looking like this:
<p align="center">
    <img src="img/taurusmainwindow_simple.png" alt="taurusmainwindow_simple" />
</p>
6. Change the label of any of the elements in the TaurusForm:
<p align="center">
    <img src="img/taurusmainwindow_simple2.png" alt="taurusmainwindow_simple2" />
</p>
7. Close the GUI. A save settings popup will appear. Click on save:
<p align="center">
    <img src="img/taurusmainwindow_simple3.png" alt="taurusmainwindow_simple3" />
</p>
8. Reopen the GUI and you will see that the label you changed is still there, as the configuration is loaded automatically.
<p align="center">
    <img src="img/taurusmainwindow_simple2.png" alt="taurusmainwindow_simple2" />
</p>

### Complex example
This TaurusMainWindow is a full GUI with a complex TaurusWidget in the center. But as it's contained in the TaurusMainWindow it also provides the extra capabilities mentioned before.  
This complex example can be found [here](examples/taurusmainwindow.py) and you can test it with _python taurusmainwindow.py_. It may look like this:  
<p align="center">
    <img src="img/taurusmainwindow_complex.png" alt="taurusmainwindow_complex" />
</p>

## Sardana-Taurus Extension widgets
Sardana provides several [taurus-based widgets](https://sardana-controls.org/users/taurus/index.html) for being used in GUIs (or, some of them, in standalone mode).  
Some custom taurus GUIs for Sardana-specific concepts like macros or measurement groups:  
<p align="center">
    <img src="https://sardana-controls.org/_images/macroexecutor01.png" alt="macroexecutor" />
    <img src="https://sardana-controls.org/_images/expconf01.png" alt="expconf" />
</p>
And some TaurusVale specializations like PoolMotorTV or PoolChannelTV:
<p align="center">
    <img src="https://sardana-controls.org/_images/pmtv.png" alt="poolmotortv" />
    <img src="https://sardana-controls.org/_images/pctv.png" alt="poolchanneltv" />
</p>