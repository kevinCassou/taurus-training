# SVG synoptic workshop


## Introduction

The word "synoptic" refers to a live, user friendly representation of a control system. Usually it is laid out in a way resembling the real system, but often in a schematic way. It can provide an overview, but also detailed views if needed.

The [svgsynoptic library](https://gitlab.com/MaxIV/lib-maxiv-svgsynoptic) is a synoptic implementation relying on the SVG image format, and PyQt. It can be connected to Tango via Taurus.

`svgsynoptic` is a fairly "low level" library that provides ways of hooking up an SVG with a python application. To use it effectively requires some knowledge of SVG, CSS and possibly javascript. On the plus side, it's very hackable.

It was initially designed to handle large synoptics for MAX IV accelerators, with 1000s of active elements that don't fit on the screen at once. Beamline synoptics are usually smaller.

![R3 synoptic zoom 1](images/r3synoptic_1.png)
![R3 synoptic zoom 2](images/r3synoptic_2.png)
![R3 synoptic zoom 3](images/r3synoptic_3.png)
![Veritas synoptic](images/veritassynoptic.png)


## Aims of the workshop

- Basic understanding of the svgsynoptic library
- How to create and modify a synoptic application
- Some idea about further possibilities


## Prerequisites

A development environment with:

- The files in the `svgsynoptic_workshop` in this git repo (https://gitlab.com/taurus-org/taurus-training.git).

- Python, pyqt5, taurus, svgsynoptic
  Using conda and the `environment.yml` file in this directory should get you there:
      
  `$ conda env create -f environment.yml`

- [Inkscape](https://inkscape.org/), a free SVG drawing program. Installers for most operating systems should be available. This is not strictly needed, but highly recommended.

- Ideally, a Tango database and standard test devices running. Not mandatory, the first part of the exercises will not require any Tango involvement.


## Exercise 1: the basics

**Note: this exercise does not depend on Tango. It is intended to show the basic workings of the svgsynoptic library.**

We will start with a very simple example.

Open a terminal, `cd` to the `svgsynoptic_workshop` directory.

If using conda (see "prerequisites" above) activate your env:

    $ conda activate svgsynoptic_workshop

Now run:

    $ python -m exercise1.app1
    
A window should open, looking roughly like this:

![Screenshot app1](images/app1_screenshot.png)

Not much to see, but some geometric shapes, and a bit of text. You can click and drag the view, and zoom using the mouse wheel. You can also click the shapes, to "select" them. But that's about it. However, it means that the synoptic library works!

Let's go through the individual files that make up this synoptic:

* `exercise1/app1.py` the python application you just started
* `exercise1/index.html` a HTML page
* `exercise1/style.css` custom CSS rules (optional)
* `exercise1/synoptic1.svg` the SVG file


### exercise1/app1.py

```python
"""
A simple example that shows how to use the SVG synoptic widget
in a stand alone application.
"""

import sys
import os

from PyQt5.QtWidgets import QApplication

from svgsynoptic2.synopticwidget import SynopticWidget


class ExampleSynopticWidget(SynopticWidget):

    """
    A custom subclass of the synoptic widget.
    """

    # Needed for now, just ignore it...
    _modelNames = None


def main():
    qapp = QApplication(sys.argv)

    # We need to give the absolute path to the HTML file
    # because our webview is setup to load assets from the
    # svgsynoptic library's path, not from the module's path.
    path = os.path.dirname(__file__)
    widget = ExampleSynopticWidget(os.path.join(path, "index.html"))
    widget.resize(800, 600)

    widget.show()
    qapp.exec_()


if __name__ == "__main__":
    main()
```

It just defines a subclass of `SynopticWidget` and starts an app to display it. The widget gets our HTML page as an argument, and that's pretty much it. The interesting parts must be in that HTML file!


### exercise1/index.html

This file is a little bigger, but most of it is "boilerplate" that you rarely modify, and we'll just look at the interesting parts here:

```html
...

<body oncontextmenu="return false;">
  
  <div id="view">                            
    <!-- this is where our synoptic will go -->
    <div class="overlay">SVG Synoptic!</div>
  </div>

  ...

  <!-- kick everything off when the page has loaded -->
  <script type="text/javascript">
                          
    window.addEventListener("load", function () {
        var container = document.getElementById("view");
        var config = {};
        
        // loadSVG is a convenience method for loading a file.  Note
        // that we need to give the absolute path to the file. ${path}
        // will be replaced with the path to this HTML file.
        loadSVG("${path}/synoptic1.svg", container, config);
    });
  </script>
  
</body>
```

Now we can see where the text in the top left of the application came from; it's not actually part of the SVG file, but in HTML. It's just there to point out that the synoptic application is really a full HTML page.

Maybe you also noticed that the text did not move when panning/zooming in the application. Positioning of the text is done using CSS, so have a look in the `style.css` file if you're interested. CSS is very powerful for styling both HTML and SVG, more about that later.

The `<script>` tag at the end is where things finally happen. Once the HTML page has loaded, we find the appropriate element to insert the synoptic into, and then load it using `loadSVG`. The `config` object is empty so far. 

If you don't know javascript, don't worry; normally you don't have to go any deeper than this. Typically a new synoptic will reuse a HTML file like this, with minor tweaks.


### exercise1/synoptic1.svg

Open the SVG file in *Inkscape* (a text editor also works, SVG is just XML).

Inkscape is a complex program and we can't go through it all here. But you should see an image looking very similar to the application you just ran.

Left-click the "star" shaped object to select it (should show a dotted box around it)

Then right-click to open the menu, and select "Object properties". A window should open up somewhere in the UI, with some basic element settings like "ID", "Label", etc.

The important thing here is the "Description" field, that should contain the following text: 

    model=star
    
This is what connects the synoptic together. The "model" is similar to the same concept in Taurus, but in this case, it doesn't have a relation to Tango and could be any string (e.g. "star"). It associates an element in the SVG with *something* in the python application.

Technically, the model is put in an invisible `<desc>` element within the element. The svgsynoptic library finds these at startup and connects them up.

Try adding another shape in Inkscape, using the tools on the left side. Maybe add some color using the palette at the bottom. 

Then add a model by editing the description, **press the "Set" button**, and save the file.

(Remember that this is a `git` repo, so if you mess up a file you can always get the original back using `git checkout <filename>`)

Running the app again, your new shape should appear. This is the basics of editing a synoptic.


## Exercise 1b: live updates

Now this is still a very boring, static synoptic. The most basic thing that you usually want a synoptic to do is to show some kind "live" information about the world. Often this is done by changing the color of elements to reflect something.

Try running the new version:

    $ python -m exercise1.app1b
    
You should see the same synoptic as before, but very soo something should start happening.

This version is using the same HTML and SVG file, the only changes are in the python file.

In the python module, we can see that some methods have been added to the class:

```python
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        QtCore.QTimer.singleShot(1000, self._update)
        self.counter = 0

    def _update(self):
        # Red every second time
        red = self.counter % 2
        classes = {"red": bool(red)}
        self.js.evaluate(f"synoptic.setClasses('model', 'star', {json.dumps(classes)})")
        self.counter += 1
        QtCore.QTimer.singleShot(1000, self._update)
```

We don't have to go into the details for this toy example, but we're setting up a Qt timer to run `_update` which does something, and then schedules itself to run again.

The `_update` method is in fact calling a *javascript* method inside the page, using `self.js` which works as a "bridge" from python to javascript. 

The `setClasses` call is a convenience function to configure CSS classes on all objects with a given model. The `json` stuff is just a convenient way to format a python dict as proper js (JSON means "javascript object notation" after all!).

In short, the application periodically adds and removes the class `red` on the "star" element, causing it to flash red. 
 
Adding the class doesn't do anything by itself, `style.css` gives it meaning:

```css
.red {
    fill: red;
}
```

So, any element that has this class will be filled with red. 


### Hardcoded styles vs CSS

Caveat: CSS rules are not very "strong", so any fill color defined withing the SVG document itself will override. Therefore it's important to make sure that the shapes we want to color this way don't have their own color set in the SVG.

Try changing the python code to instead change the color of the shape you added to the SVG file before, by referring to your model. It may not work, for the above reason. 

Let's fix it. In Inkscape, select your shape, right click and select "Fill and stroke...". In the window pick the "Fill" tab, and click the "?" button. That should "unset" the fill of the object (also turning it completely black). Save, and try again. Hopefully it works better now!

If you still have problems getting rid of the fill, you can open the SVG file in a text editor, locate your element (e.g. by searching for `model=yourmodel` to find the `<desc>` element, and locating its parent element) and check if its `style` attribute contains something like `fill=#cccccc;`. If it does, delete that part, and save the file.

To solve the "problem" with things being all black when no color is set, you could add a CSS rule in `style.css`, which sets a default fill color for all SVG elements:
```css
svg {
    fill: green;
}
```
That will make SVG elements green by default. The more specific rule using the "red" class will override when it's present.

CSS is a very powerful tool for customizing your synoptic. There's loads of information on the web on how to use CSS to style HTML and SVG.


## Exercise 1c: interactions

Having information updating in the synoptic is great, but what about if the user needs to interact with it? Maybe they want to be able to click something to open/close a valve or whatever they do.

`app1c.py` adds some more functionality. Run it, and click the gray rectangle while watching the terminal output. Apart from some "JsConsole" debug output, the text `*** Box was clicked! ***` should be printed each time.

This is accomplished by the following new method:

```python
    def on_click(self, kind, name):
        # Overriding the left click event handler
        if kind == "model" and name == "box":
            print("*** Box was clicked! ***")
        else:
            super().on_click(kind, name)
```

Whenever an element that has a `model` is clicked in the synoptic, a javascript event handler is triggered, and sends a message across the "bridge" back to the python widget. It has a default handler method called `on_click` that handles the selection logic. But we can override it to do something else.

Here, we detect whether the box element was clicked, and in that case print a message, otherwise we revert to the default behavior. This also means that it's no longer possible to "select" the box.

This is the basics for making the synoptic interactive. You could of course do something more useful such as acting on the control system, opening other windows, etc. There is also a `on_rightclick` method for catching right mouse button clicks.

*This concludes exercise 1!* By now you have now seen all the "moving parts" in a synoptic, and hopefully have some feeling for how they fit together. 


## Exercise 2: time to Tango

**Note: this exercise requires that you have access to a Tango control system from your machine. It also assumes you have a standard TangoTest device running.**

Try running this application:

    $ python -m exercise2.app2

You should see something like this:

![Screenshot](./images/app2_screenshot.png)

There should be a bit more life in this synoptic, at least a number updating regularly. If you hover the mouse ponter over the value, it should tell you that it's the `sys/tg_test/1/doule_scalar` attribute, a test attribute that is always changing. Also, one of the circles should have a color. It shows the value of `sys/tg_test/1/boolean_scalar`.

The color of the large box represents the current `State` attribute of the same test device, and hovering over it should tell you exactly which state it is in.

Try changing writing the value of the `boolean_scalar` value (e.g. using Jive) and check that the circle's color changes accordingly.

You can also set a unit on the `double_scalar`, it should display in the synoptic. Or change the format.

Try right-clicking the boxes on the right. A standard Taurus device panel should appear.


### exercise2/app2.py

Let's have a look in the python module. It looks very similar to the one in Exercise 1, but some things are different:

```python
class ExampleSynopticWidget(TaurusSynopticWidget):

    """
    A custom subclass of the synoptic widget.
    """
    

def main():
    qapp = Qt.QApplication([])

    path = os.path.dirname(__file__)
    widget = ExampleSynopticWidget()
    widget.setModel(os.path.join(path, "index.html"))
    widget.resize(1000, 700)
    widget.show()
    qapp.exec_()

```

We are now inheriting from a different widget baseclass, `TaurusSynopticWidget`. This one adds Taurus functionality to the basic `SynopticWidget`, turning it into a Taurus widget. That is also why the call below uses `setModel` to configure the HTML file to load. Otherwise it looks very similar. Really all the difference is that our synoptic now understands Taurus models!

The HTML file is almost identical to the Exercise 1, apart from loading a different SVG file.


### exercise2/synoptic2.svg

Opening the SVG in Inkscape, you can immediately see that there's something there that doesn't show up in the application, namely an extra icon. More about it later.

You should now be able to find the models responsible for this, in the object description just like before. The models look like this:

    model=sys/tg_test/1
    model=sys/tg_test/1/boolean_scalar
    model=sys/tg_test/1/double_scalar

This is probably not very surprising to you if you have used Taurus before; these look like Taurus models, really just Tango device and attribute names.

As you can see, we can add an attribute model to a `text` element, and the contents will be replaced by "live" attribute values from the corresponding attribute.

Setting the model to a device name will automatically use the "State" attribute for that device to set the fill color of the element. You get the same effect by explicitly setting the State attribute as model, but the nice thing about just using device name is that it works with the right click feature mentioned above, to open device panels.

Try setting some other models on the other elements in the SVG. Generally, the library should handle most scalar values, but higher dimensional ones probably won't display well.

Open the layer dialog (meny -> Layer -> Layers...). You should see that there are now two sub-layers to the main layer; "symbols" and "background". More about those in Appendix 2. Also, here is an example of *clones* (Appendix 4); the icon at the top is invisible because it's in the "symbols" layer, and the visible version of it is a "clone". You can see this by selecting the elements, it should show at the bottom of the window.

You could try to add your own elements, but make sure to put them in the "background" layer. Also bear in mind the advice about "fill" from Exercise 1.


### Models

Since the synoptic uses Taurus under the hood, you can also try more complex models. For example: `model=eval:{sys/tg_test/1/float_scalar} + {sys/tg_test/1/short_scalar}`.


### CSS 

All the colors and styles associated with Tango states and qualities, as well as boolean values are defined in CSS, and can be overridden in `styles.css`. Probably you don't want to change the standard Tango state colors, that would be confusing. But you can see an example of how to override the color for boolean "true" values.

You can do quite a lot of interesting things with simple rules like this, using the power of CSS selectors. It goes beyond this introduction, but since the attribute value is available as a "data-value" attribute on the element, it can be matched against. The "quality" is available as "data-quality". It's even possible to define animations in pure CSS!

Try changing the color for `true` values in `styles.css`, or change it to catch `false` values instead.


## Exercise 2b: running a command

Time to add a bit of interactivity!

Here's the new method added in `app2b.py`.

```python
    def get_device_panel(self, device):
        dev = Device(device)
        proxy = dev.getDeviceProxy()
        info = proxy.info()
        if info.dev_class == "TangoTest":
            proxy.command_inout("SwitchStates")
        else:
            return super().get_device_panel(device)
```

The `get_device_panel` method can be overridden to let our taurus synoptic open custom widgets when devices are right clicked. By default, a generic `TaurusDevicePanel` is returned, but we could also do something else; maybe we don't want to open a panel but instead perform some action for certain devices. 

Here, we run the `SwitchStates` command if a `TangoTest` device is clicked. Also we return nothing, to prevent any panel from opening.

This is all quite primitive, e.g. probably we should do all this in a thread in order to not block the UI. But hopefully you get the picture!

You could of course also just use the `on_rightclick` method like above, by default it calls the `get_device_panel` for you when the model looks like a device name. But it's good to know about the different options, when you need to customize behavior (which is very common.)

The synoptic widget also tries to be smart about opening panels, so if a device panel is already open, it should just focus it instead of opening another identical panel.


## Exercise 2c: custom device widgets

It might be a bit dangerous to run commands "silently" like in the previous exercise. It won't be obvious to the user what happens when right clicking in this case, or indeed that it does anything at all! However the default widget panel is not very convenient.

In `app2c.py` we import a widget from `widgets.py`, called `SwitchStatesPopup`. The idea is to open a small window just under the mouse pointer, giving quick access to some convenient command. Here we hook it up to the right click just like before. This example might serve as basis for more elaborate "popup" widgets, for e.g. opening/closing valves. With a bit of logic in the `get_device_panel` method you can return completely customized widgets for any device, recognized by class, name or whatever else.

Try adding some other commands ("Init" maybe?) to the popup widget.

Try to return a different command popup for some other devices you have available.

OK that's it for this workshop. Feel free to have a look at the appendices, that cover some further functionality.


## Appendix 1: the inspector

It can be very frustrating to hunt down some broken CSS rule or misspelled class in a synoptic. But fear not, just press F12 to call on the Inspector! It's basically the same thing as the "developer tools" available in modern web browsers, and a very advanced tool.

The most useful feature is probably the little arrow in the top left corner, that allows you to click any element in the synoptic to inspect it. But you also have access to the javascript console to execute arbitrary javascript inside the page, as well as a debugger and performance tools if you really need it.


## Appendix 2: layers

A real synoptic is likely to contain quite a lot of stuff eventually, as people want to include more and more information. `svgsynoptic` supports some ways to handle this, the simplest one being "layers".

Using layers means you can put elements into several different groups, and toggle the display of them independently. Imagine for example a beamline synoptic where information about vacuum, cooling, motorization and so on are separated into layers. Now someone interested only in vacuum could just turn off the other layers for a less distracting view.

Layers are quite convenient to use since they correspond to layers in Inkscape. You can see the layers in a file by opening the menu item "Layer -> Layers...". The layers are easily rearranged using drag and drop in the layers dialog. See the rules below.

In order to be able to toggle layers, the `layers.js` plugin must also be loaded in the HTML file.


### Rules for layers

* There must always be one "root" or "main" layer, which all other layers are "sublayers" of. Its name doesn't matter, and in inkscape there is always one layer at the beginning.

* There should be at least one sublayer of "root", inside which your elements should be placed.

* You can draw things outside of the layers, but the synoptic won't recognize models defined there, and other features may not work.

* Only sublayers of the "root" are considered layers in the synoptic.

* If there's a layer named "background", it will always be visible.

* Any layer called "symbols" is always invisible. It's useful for keeping icons and things to be re-used in the synoptic.

There's an example application in `appendix2/` showing the structure. There are a couple of lines that must be present in the HTML file, in order to enable the layer selector plugin.

```html
<link rel="stylesheet" href="css/layers.css" />
...
<script src="js/layers.js"></script>
```

### Tips

* Confused about what is where? It's easy to find out in inkscape where your element is; it's displayed at the bottom when the element is selected. You can also open the "Layers..." window and toggle visibility of layers in order to figure out what's where.

* Mind the order of layers, as it determines what gets drawn on top. Probably you want the "background" layer to be below the other layers.


## Appendix 3: zoom levels

You probably noticed that it's possible to zoom the synoptic with the mouse wheel. It's not particularly useful in a tiny synoptic as everything fits on the screen. But in a large synoptic it may not even be possible to fit everything at once. 

For those cases, instead we have implemented a way to segment the view into "zoom levels", which work somewhat like layers, but instead of the user turning them on and off using buttons, they go in and out of view depending on the current scale of the synoptic.

Using zoom levels is also good for performance since it allows the synoptic to only subscribe to Tango attributes for elements that are currently visible.

Zoom levels are implemented as sublayers of a layer, with special names, e.g. "zoom0", "zoom1" and so on. The actual scales at which the synoptic will switch between zoom levels are configured in the HTML file. With some tweaking it's possible to have multiple different levels of detail.

Look in the example application in `appendix2` to see how this works. The blue boxes should change into lots of smaller boxes when zooming in. The layer called "Things" contains two diffent zoom levels. The `zoomSteps` configuration in the call to `loadSVG` controls at what scale to switch between zoom layers.


## Appendix 4: groups and clones

If you want to go beyond simple geometric shapes in your synoptics, and draw more elaborate icons, you probably want to use "groups". Selecting several elements and selecting "Object -> Group" from the menu combines these into a convenient unit. You can set a model on the group, and any elements inside it will be affected. Just make sure to remove the "fill" on the elements you want to be colored.

However, it's usually not a good idea to group things with models together, as this tends to mess with the transform styles. If you see strange problems with elements not being updated, check if they are inside a larger group, and remove it.

Another very convenient feature when drawing larger synoptics is "clones" (Object -> Clone). By cloning an element (works on groups too) you create copies that are still linked to the original, so that editing the original also updates all clones. The clones can of course still have different models. 

You should never set a model on the original, so only use clones in the synoptic. The hidden "symbols" layer is a good place to keep the originals for your clones.

The `exercise2` example contains an example of a cloned icon.
