"""
A simple example that shows how to use the SVG synoptic widget
in a stand alone application.
"""

import json
import sys
import os

from PyQt5.QtWidgets import QApplication
from PyQt5 import QtCore

from svgsynoptic2.synopticwidget import SynopticWidget


class ExampleSynopticWidget(SynopticWidget):

    """
    A custom subclass of the synoptic widget.
    """

    # Needed for now, just ignore it...
    _modelNames = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        QtCore.QTimer.singleShot(1000, self._update)
        self.counter = 0

    def _update(self):
        # Red every second time
        red = self.counter % 2
        classes = {"red": bool(red)}
        self.js.evaluate(f"synoptic.setClasses('model', 'star', {json.dumps(classes)})")
        self.counter += 1
        QtCore.QTimer.singleShot(1000, self._update)

    def on_click(self, kind, name):
        # Overriding the left click event handler
        if kind == "model" and name == "box":
            print("*** Box was clicked! ***")
        else:
            super().on_click(kind, name)


def main():
    qapp = QApplication(sys.argv)

    # We need to give the absolute path to the HTML file
    # because our webview is setup to load assets from the
    # svgsynoptic library's path, not from the module's path.
    path = os.path.dirname(__file__)
    widget = ExampleSynopticWidget(os.path.join(path, "index.html"))
    widget.resize(800, 600)

    widget.show()
    qapp.exec_()


if __name__ == "__main__":
    main()
