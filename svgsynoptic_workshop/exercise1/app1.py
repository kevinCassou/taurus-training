"""
A simple example that shows how to use the SVG synoptic widget
in a stand alone application.
"""

import sys
import os

from PyQt5.QtWidgets import QApplication

from svgsynoptic2.synopticwidget import SynopticWidget


class ExampleSynopticWidget(SynopticWidget):

    """
    A custom subclass of the synoptic widget.
    """

    # Needed for now, just ignore it...
    _modelNames = None


def main():
    qapp = QApplication(sys.argv)

    # We need to give the absolute path to the HTML file
    # because our webview is setup to load assets from the
    # svgsynoptic library's path, not from the module's path.
    path = os.path.dirname(__file__)
    widget = ExampleSynopticWidget(os.path.join(path, "index.html"))
    widget.resize(800, 600)

    widget.show()
    qapp.exec_()


if __name__ == "__main__":
    main()
