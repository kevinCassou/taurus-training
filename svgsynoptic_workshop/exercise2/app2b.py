"""
A simple example that shows how to use the SVG synoptic widget
and connect it to a Tango control system using Taurus.
"""

import os

from taurus import Device
from taurus.external.qt import Qt
from svgsynoptic2.taurussynopticwidget import TaurusSynopticWidget


class ExampleSynopticWidget(TaurusSynopticWidget):

    """
    A custom subclass of the synoptic widget.
    """

    def get_device_panel(self, device):
        dev = Device(device)
        proxy = dev.getDeviceProxy()
        info = proxy.info()
        if info.dev_class == "TangoTest":
            print("Running a command")
            proxy.command_inout("SwitchStates")
        else:
            return super().get_device_panel(device)


def main():
    qapp = Qt.QApplication([])

    path = os.path.dirname(__file__)
    widget = ExampleSynopticWidget()
    widget.setModel(os.path.join(path, "index.html"))
    widget.resize(1000, 700)
    widget.show()
    qapp.exec_()


if __name__ == "__main__":
    main()
