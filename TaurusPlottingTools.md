# Plotting tools
Taurus also includes some plotting tools in order to represent data arrays and values on plots.

## Pre-requisites
In order to do the following exercises you should have a taurus environment set up, you can follow the instructions on the [README.md](README.md) to install it.

## Taurus Plot
A Taurus plot leverages Taurus's capabilities to visualize data. 
These plots are often used in scientific and industrial contexts where real-time monitoring of data is crucial. 
Taurus plots can display a variety of data types.

### Exercise 1: Plot data using the command line:
1. Open a terminal and type: `taurus plot "eval:Q(rand(333),'mm')"`
2. Now repeat the above but add a tango array attribute: `taurus plot "eval:Q(rand(333),'mm')" sys/tg_test/1/wave`

As you can see plotting attributes using taurus_pyqtgraph is easy and effortless thanks to the taurus framework.

## TaurusTrend
TaurusTrend is a widget provided by the Taurus framework, specifically designed for plotting time-based data trends in real-time.

### Exercise 1: Plot time data using the command line:
1. Open a terminal and type: `taurus trend "eval:rand()"`
2. Now repeat the above but add a tango scalar attribute: `taurus trend "eval:rand()" sys/tg_test/1/double_scalar_rww`

## Exploring taurus_pyqtgraph capabilities
Now that we know the basics on how to plot data we can explore what options has TaurusTrend and TaurusPlot:

### Exercise 1: Execute a taurus trend with a random eval and a Tango attribute:
To do it type: `taurus trend "eval:rand()" sys/tg_test/1/double_scalar_rww`

### Exercise 2: Explore the x_axis menu

![](img/x_axis.png "X axis Submenu")

1. Make a right-click on the taurus trend and select 'X axis' submenu on the contextual menu.
2. A description on the functions can be read at the final of this exercise.
3. You can modify the time range you want to look at by doing it manually on the 'Manual' option or even better, using the 'Set View Range' editable dropdown menu. This dropdown menu have some predefined values, but you can edit it to the time you want by following the convention, don't worry if you make a syntax mistake there will be a message box with the syntax expected. Try to set the range to 5 minutes.
4. The time range option can be found on the plot configuration menu as well.

#### Options on the X_axis submenu:
 + Fixed range scale: It fixes the visible time range on the trend in order to keep just a slice of data visible to the user.
 + Manual: It allows to select a customizable time range.
 + Auto: It allows to select the % of data visible to the user, the options 'Visible Data Only' and 'Auto Pan Only' make this % more accurate depending on your needs.
 + Invert Axis: It inverts the axis direction.
 + Mouse Enabled: It prevents the axis to be moved by the mouse.
 + Link Axis: It allows the axis to be moved to a defined ViewBox, by default there is only one.
 + Set View Range: As explained on the exercise you can select predefined time ranges or type a custom one.
 + Log Scale: It allows to set the axis to logarithmic mode, which in that case is not useful, but it could be interesting for some scenarios.


### Exercise 3: Explore the y_axis menu

![](img/y_axis.png "Y axis Submenu")

1. Make a right-click on the taurus trend and select 'Y axis' submenu on the contextual menu.
2. A description on the functions can be read at the final of this exercise.
3. Select the option 'Log Scale'. You will see that data is now represented with a logarithmic scale (the gaps on the tango attribute are normal due to the fact that log(0) is not a valid operation)
4. Now deselect 'Log Scale' to restore the visualization.

#### Options on the Y_axis submenu:
 + Manual: It allows to select a customizable range between the values.
 + Auto: It allows to select the % of data visible to the user, the options 'Visible Data Only' and 'Auto Pan Only' make this % more accurate depending on your needs.
 + Invert Axis: It the axis direction.
 + Mouse Enabled: It prevents the axis to be moved by the mouse.
 + Link Axis: It allows the axis to be moved to a defined ViewBox, by default there is only one.
 + Log Scale: It allows to set the axis to logarithmic mode, which in that case is not useful, but it could be interesting for some scenarios.


### Exercise 4: Explore the mouse mode option

![](img/mouse_mode.png "Example on how to use the 1 button mode for the mouse")

1. Make a right-click on the taurus trend and select 'Mouse mode' submenu on the contextual menu.
2. Here you have two options, the 3 button mode and the 1 button mode, try it out, explanation can be found at the end of this exercise.
3. For the rest of the session set it to 3 button mode.

#### Options on the mouse mode submenu:
 + 3 button: Acts with drag mode, you can drag the trend or the plot to the direction you want.
 + 1 button: Acts with a range selector, you can select what data you can view with a rectangular selection.


### Exercise 5: Explore the Change curves titles option

![](img/change_titles.png "Change curves titles option")

1. Make a right-click on the taurus trend and select 'Change curves titles' submenu on the contextual menu.
2. A pop-up will appear with an editable dropdown, you can use the patterns from the dropdown and combine some of them. In this case type: _{dev.name}/{attr.label}_
3. You will notice that the legend has changed.
4. You can modify the curves titles from the 'Model selection' menu.

### Exercise 6: Explore the model selection tool
The model selection tool in taurus_pyqtgraph works the same way as Taurus, you can have a look at the Taurus exercises for more information.
However, we have some extra options in the case of taurus plot and taurus trend.

![](img/model_selector.png "Model selector window")

1. Make a right-click on the taurus trend and select 'Model selection tool' submenu on the contextual menu.
2. The left part of the window is the source data selector, this has been explained at the taurus exercises, but the right part is unique for taurus trend and taurus plot.
3. You can see that we have the source and the title of each curve, you can add, delete and edit curves.
4. Here you can change the curves titles by clicking the 'Change Curves Titles' button, the functionality is the same as the one that is at the contextual menu.


### Exercise 7: Explore the calculate statistics option

![](img/calculate_statistics.png "Calculate statistics option")


1. Make a right-click on the taurus trend and select 'Calculate Statistics' option on the contextual menu.
2. Here you can explore all the statistics calculated for each curve that you have on your taurus trend.
3. Try to hide some of them by clicking on the checkbox.
4. Anytime you can re-calculate the statistics by clicking on the bottom button.

### Exercise 8: Explore the plot configuration option

![](img/plot_configuration.png "Plot configuration option")


1. Make a right-click on the taurus trend and select the 'Plot Configuration' option on the contextual menu.
2. Try to change the line style of one of the curves, by clicking on the desired curve and using the dropdown menu.
3. Try to change thw color, and style of symbols and click the apply button under the curves.
4. Now open the plot configuration again, select the first curve and change the axis to Y2, then click apply and close the window.
5. Toy will see  that now the curves are separated and you will have 2 axis, like the image below:

![](img/different_axis.png "Curves on different axis")


Play with all the other options to modify the trend to you needs, there are a lot of options.

### Exercise 9: Explore the Data inspector mode

![](img/data_inspector.png "Data inspector mode")


1. Make a right-click on the taurus trend and select 'Data inspector' checkbox on the contextual menu.
2. If you move the mouse along the curves you will see a tooltip for each curve and point that it has.
3. To disable the inspector mode you have to deselect the 'Data inspector' checkbox on the contextual menu.

### Exercise 10: Explore Change forced read period option

![](img/change_read_period.png "Change force read period pop-up")


The force reading period refers to the interval at which the library forcibly reads data from the data source, even if there hasn't been an explicit change or event triggering a read.
With this option we can control the amount of points that are plotted.

1. Make a right-click on the taurus trend and select 'Change forced read period' option on the contextual menu.
2. A pop-up will appear, then you can type the 'polling period' and click 'OK', for testing purposes type 500.
3. Every half a second a new point will appear.
4. If you want to disable it type 0 again and the polling will be disabled.

### Exercise 11: Explore Change buffers size option

![](img/change_buffer.png "Change buffer size pop-up")


The buffer data size is the maximum number of points that will be kept in memory for each curve.

1. Make a right-click on the taurus trend and select 'Change buffers size' option on the contextual menu.
2. Change the buffers size to 500, you will see that the number of points will be decreased once the limit is reached.

### Exercise 12: Explore the Export tool

![](img/export.png "Export data pop up")


The export option is designed to transform the plot to other formats, a detailed list of formats can be found at the end of this exercise.

1. Make a right-click on the taurus trend and select 'Export' option on the contextual menu.
2. Select the item named 'Plot'.
3. Select the option 'CSV' on 'Export format'.
4. Click the export button, you will be prompted with the desired path to store the csv file.
5. Open the file and check that the csv file has been generated correctly.

![](img/export_result.png "Visualize the result of the export")


#### Options on the export tool:
 + CSV: It exports the data using comma separated values to a file. It can be configured to change the separator and the precission.
 + HDF5: It exports the data using the HDF5 file format.
 + Matplotlib Window: It shows the data with matplotlib.
 + SVG: It exports the data using an image in the SVG format. (Not working properly right now)
 + ASCII: It exports the data using the Taurus 4 compatible ASCII format.

### Exercise 13: Real use case for monitoring memory usage of a process
Combining taurus trend with eval we can get the memory usage and get a trend with the current status.

![](img/example_1.png "Monitor the memory usage of a process using eval ")

1. First of all select a running process id from the ones that are running on your pc, to do it you can type top and anotate the one that has more memory usage for example.
2. Now that you have the PID of the process you want to monitor, you can monitor the memory usage by typing: `taurus trend -r500 "eval:@psutil.*/Process(REPLACE_THIS_BY_PID).memory_info().rss/2**10"`

## Real use case integrating  archiving
Taurus trend has the possibility to plot archiving data by selecting it on the contextual menu, but this option just appears if you have pyhdbpp installed and you have done the previous configuration.

Here you can see a GIF with the archiving option working, and if you want to have a look at it you can check out the [TangoBox OVA](https://gitlab.com/tango-controls/tangobox) and this [TangoBox issue](https://gitlab.com/tango-controls/tangobox/-/issues/57) to check how to configure it.

## Existing application using taurus_pyqtgraph
There are a lot of applications (most of them are on internal repositories), but there is a new one named [TangoBrowser](https://gitlab.com/tango-controls/hdbpp/libhdbpp-tangobrowser) that have been published and can be installed.

Tango browser is a tango and archiving tool to look for attributes in use or ever archived.
This tool is a graphical interface to search tango attributes archived or not.

Due to the fact that this GUI have been developed using taurus you can drag&drop from the list above to the taurus trend and it will start to plot the attribute.

![](img/tango_browser.png "TangoBrowser application with a search and a trend with data")


### Extra tip: Auto arrange symbol
Did you notice the small button at the left-bottom of the taurus trend or taurus plot?

If you click on it the widget will make the data fit on the widget.