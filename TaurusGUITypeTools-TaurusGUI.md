# Taurus GUI Tutorial

Taurus provides a powerful framework for building graphical user interfaces (GUIs) that interact with control systems. This tutorial will guide you through creating a Taurus GUI using `taurus newgui`, exploring its components, and understanding how to use them effectively.

## Creating a New Taurus GUI

1. **Generate a New GUI**:
    To create a new GUI, use the `taurus newgui` command. Open a terminal and run:
    ```
    taurus newgui
    ```
    A wizard tool will be launched.

    <p align="center">
    <img src="img/taurus_gui_1.png" alt="TaurusGUI" />
    </p>
    - Select project directory.
    <p align="center">
    <img src="img/taurus_gui_2.png" alt="TaurusGUI" />
    </p>
    - Write a GUI name:
    <p align="center">
    <img src="img/taurus_gui_3.png" alt="TaurusGUI" />
    </p>
    - Select logo --> Next<br/>
    - Synoptics --> Next<br/>
    - Macro Server Info --> Next<br/>
    - Panels editor: Create a panel with a TaurusForm and other with TaurusTrend with attributes from `sys/tg_test/1`:<br/>
        <p style="margin-left:2rem">
        - Click on `Add Panel`<br/>
        - Write panel name: `Panel1`<br/>
        - Select Form widget<br/>
        </p>
        <p align="center">
        <img src="img/taurus_gui_5.png" alt="TaurusGUI" />
        </p>
        - Push `Advanced settings...`
        - Select a model:
        <p align="center">
        <img src="img/taurus_gui_6.png" alt="TaurusGUI" />
        </p>
        - Click on finish. Panel willbe added.
        <p align="center">
        <img src="img/taurus_gui_4.png" alt="TaurusGUI" />
        </p>
        - Next
    - External Applications: Add application Kcalck<br/>
    - Monitor List --> Next<br/>
    - Confirmation Page --> Finish.<br/>
    Instruction to install it must be shown:
    <p align="center">
    <img src="img/taurus_gui_7.png" alt="TaurusGUI" />
    </p>

    After installing the GUI, you can drop panels inside it and close _TaurusManual_. You can find a launcher for kCalc too. Something similar to this will happen:
    <p align="center">
    <img src="img/taurus_gui_8.png" alt="TaurusGUI" />
    </p>


2. **Project Structure**:
    Navigate to the directory to explore the generated files:
    ```
    cd DIRECTORY
    ```
    The directory structure will look like this:
    ```
    DIRECTORY/
    ├── tgconf_mytaurusgui
    │   ├── __init__.py
    │   ├── config.py
    │   └── config.xml
    ├── MANIFEST.in
    ├── wizard.log
    └── setup.py
    ```

3. **Understanding the Components**:

    - **`config.py`**: File that link to XML file to load default GUI configuration.
    - **`config.xml`**: File that contains the description of the GUI panels. we can see the _PanelName_, _ClassName_, _model_, etc.
    - **`__init__.py`**: Makes the directory a package.
    - **`MANIFEST.in`**: Commands instructing setuptools to add or remove some set of files. In our case, add all files under GUI directory.
    - **`setup.py`**: Script for installing the project.
    - **`wizard.log`**: Log of the GUI wizard.

4. **Running the GUI**:
    To run the GUI, execute the following command in the terminal:
    ```
    mytaurusgui
    ```
    This will launch the GUI application.

    Organize the panels to they are all docked within the main window.

#### Populating the GUI with additional panels

The Panel1 with the form we just created is not modifieble at runtime.
So, you can not modify its content e.g. add/remove models.
    <p align="center">
    <img src="img/taurus_gui_9.png" alt="TaurusGUI" />
    </p>

1. **Adding Temporary Panels**
    More panels can be added at runtime of a Taurus GUI.
    First, you will need to *Lock View* with an option from the *View* menu.
    <p align="center">
    <img src="img/taurus_gui_10.png" alt="TaurusGUI" />
    </p>
    Then you can use the New Panel action from the toolbar which will open you
    a wizard (exactly the same wizard as when creating a new GUI) to add a new panel.
    <p align="center">
    <img src="img/taurus_gui_11.png" alt="TaurusGUI" />
    </p>
    So, let's add a new Panel2 with a form pointing to another attribute of `sys/tg_test/1` device.
    <p align="center">
    <img src="img/taurus_gui_12.png" alt="TaurusGUI" />
    </p>

    This is a temporary panel. So when you close the application the panel will be lost. Try it.

2. **Converting Temporary Panels into Permanent Panels**
   Temporary Panels may be converted into Permanent Panels, in order to not loose panels between application
   restarts, either when closing the application, or using the *Switch temporary/permanent status*
   action from the *Panel* menu. Using this second option you can also change the Permanent Panel into
   a Temporary one.
   <p align="center">
    <img src="img/taurus_gui_13.png" alt="TaurusGUI" />
   </p>

   Permanent panels are stored in the Qt settings file.
   The setting file is created (or updated) after closing the application and can be explored
   using the config browser app:
   ```
   taurus config ~/.config/TAURUS/MyTaurusGUI.ini
   ```
   <p align="center">
    <img src="img/taurus_gui_14.png" alt="TaurusGUI" />
   </p>

   Note the `ModelInConfig` setting difference between the Panel1 (created with the wizard) and the Panel2 (permanent panel).

3. **Export current Panel configuration to XML**
   Panels created with the wizard are stored in the XML file. The XML is fail-proof of eventual
   incompatibilities between Qt version or even Taurus version changes (the second one are clearly bugs).
   The information stored in the XML is however less complete than the one in the Qt settings
   The XML contains the model names and few configuration settings, while the Qt settings contains the Qt specifics
   and advanced Taurus settings like the TaurusValue sub-widgets class names. Hence both files are complementarly
   used for XML panels.

   In order to export panels into the XML file, you need to use the option *Export current Panel configuration to XML*
   from the *Tools* menu.
   <p align="center">
    <img src="img/taurus_gui_15.png" alt="TaurusGUI" />
   </p>

#### Storing GUI layout in perspectives

1. **Storing current layout as a perspective**
   Different tasks often require different application layouts and different control widgets to be at hand
   (e.g., casual use VS expert use, or equipment calibration VS equipment operation).
   TaurusGui-based applications typically provide a set of pre-defined perspectives which are just configurations
   that can be loaded at any moment to set a given panel arrangement suited for a given task.

   All TaurusGui based applications provide a Perspectives Toolbar (if it is not shown,
   you can enable it from View->Toolbars, or you can access its functionalities from the View menu).

   Use the *Save perspective* option from the Toolbar to save the current layout of the application
   as a new perspective:

   <p align="center">
    <img src="img/taurus_gui_16.png" alt="TaurusGUI" />
   </p>

   Modify the layout and store it again as a another layout:

   <p align="center">
    <img src="img/taurus_gui_17.png" alt="TaurusGUI" />
   </p>

   Use the load perspective to go back to the previous layout.

#### Other Taurus GUI features

   See documentation of:
   - [synoptic panels](https://taurus-scada.org/users/ui/taurusgui.html#synoptic-panels)
   - [Sardana integration (Macroserver & Pool)](https://taurus-scada.org/users/ui/taurusgui.html#sardana-integration-macroserver-pool)
