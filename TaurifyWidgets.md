# Taurify Widgets

## Introduction
In some cases neither Taurus nor other third party modules provide widgets with the required functionality for your GUIs, and a new widget needs to be created. If such widget requires to interact with a control system or other data sources supported via Taurus model objects, the recommended approach is to create a widget that inherits both from a QWidget (or a QWidget-derived class) and from the taurus.qt.qtgui.base.TaurusBaseComponent mixin class (or one of its derived mixin classes from taurus.qt.qtgui.base). These Taurus mixin classes provide several APIs that are expected from Taurus widgets, such as:
- Model support API
- Configuration API
- Logger API
- Formatter API

For this reason, this is sometimes informally called “Taurus-ifying a pure Qt class”.

>If you create a generic enough widget which could be useful for other people, consider contributing it to Taurus, either to be included directly in the official taurus module or to be distributed as a Taurus plugin.

## PowerMeter
The following widget is a an example of creating a Taurus “power-meter” widget that displays the value of its attached attribute model as a bar (like e.g. in an equalizer). For this we are going to compose a QProgressBar with a taurus.qt.qtgui.base.TaurusBaseComponent mixin class.  
The mixin class provides all the taurus fucntionality regarding setting and subscribing to models, and all one needs to do is to implement the handleEvent method that will be called whenever the attached taurus model is updated.  
Moreover, in the [example](examples/powermeter.py) we can find 2 PowerMeter widgets.
- The first one is simply a regular powermeter with random values. You can try it with _python powermeter.py --single-model_.
- The second one is the same powermeter but using model-composer. You can try it with _python powermeter.py_.

### Multi-model support: model-composer
The taurus base classes support multiple models. The main difference with a single-model is the need to define the supported model keys in the .modelKeys class method and then handle the different possible sources of the events received in handleEvent. They you can setModel as usual but with the extra keyword "key" that needs to be one of the keys defined in modelKeys.  
Note that the first key in modelKeys is to be used as the default when not explicitly passed to the model API methods.  
You can compare the two PowerMeter classes in the [powermeter example](examples/powermeter.py) to see these differences.
<p align="center">
    <img src="img/powermeter_single.png" alt="powermeter_single" />
    <img src="img/powermeter_multi.png" alt="powermeter_multi" />
</p>

## Table
There is another exmaple to Taurify a widget using TaurusBaseComponent. Example is named [tablewidget.py](examples/tablewidget.py) and you can find a custom QTableWidget Taurufied.

## Custom Radio Button
There is no widget in taurus to display/select values with radio buttons. For a single selection widget, you can use a TaurusValueComboBox, but a group of RadioButtons can be more usable for a small set of options.  
This [taurified radio buttons widget](examples/radiobuttons.py) accepts a list of options (in a dict of "radio button text":value) and creates a radio button for each option. You can try it by running _python radiobuttons.py_.  
It should look like the next image. The initial selected value depends on the current sys/tg_test/1/ampli value. If no value is selected it means that the value is not between 0 and 3:  
<p align="center">
    <img src="img/radiobuttons.png" alt="radiobuttons" />
</p>